Linkkick::Application.routes.draw do
  devise_for :users
  resources :collections
  resources :links
  get '/:identifier' => 'collections#show'
  root 'collections#new'
end
