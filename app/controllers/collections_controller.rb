class CollectionsController < ApplicationController
  def new
    @collection = Collection.new
    @collection.links.build
  end

  def create
    if current_or_guest_user.collection
      @collection = current_or_guest_user.collection
      @collection.links.build(collection_params[:links_attributes]['0'])
    else
      @collection = Collection.new(collection_params)
      @collection.generate_identifier
      @collection.user = current_or_guest_user
    end
    @collection.links.last.user = current_or_guest_user
    if @collection.save
      flash[:notice] = "Tell your friends to go to here: <a href='#{@collection.url(request.host)}'>#{@collection.url(request.host)}</a>"
      redirect_to action: 'show', identifier: @collection.identifier
    else
      flash[:notice] = 'Oops! There was a problem. Try again with a valid url.'
      render :new
    end
  end

  def update
    @collection = Collection.find(params[:id])
    @identifier = @collection.identifier
    if @collection.update_attributes(collection_params)
      flash[:notice] = "It worked! Here's your new url: <a href='#{@collection.url(request.host)}'>#{@collection.url(request.host)}</a>"
      redirect_to "/#{@collection.identifier}"
    else
      flash[:notice] = 'Oops! There was a problem. Try again with a different name.'
      redirect_to "/#{@identifier}"
    end
  end

  def show
    @collection = Collection.find_by_identifier(params[:identifier])
    if @collection
      @links = @collection.links.order('created_at DESC')
    else
      redirect_to action: 'new'
    end
  end

private

  def collection_params
    params.require(:collection).permit(:identifier, links_attributes: [:url])
  end
end
