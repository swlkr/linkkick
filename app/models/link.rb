class Link < ActiveRecord::Base
  belongs_to :user
  belongs_to :collection

  validates :url, presence: true
  validate :url_valid?

  def url_valid?
    URI.parse(url).kind_of?(URI::HTTP)
  rescue URI::InvalidURIError
    errors.add :url, "needs to be real"
  end
end
