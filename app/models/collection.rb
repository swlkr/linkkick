class Collection < ActiveRecord::Base
  belongs_to :user
  has_many :links
  accepts_nested_attributes_for :links

  validates :identifier, uniqueness: true
  validates :identifier, presence: true
  validates :identifier, format: { with: /\A[-a-zA-Z]+\z/ }
  validate :has_link?

  before_validation :remove_whitespace

  def remove_whitespace
    identifier.strip
  end

  def has_link?
    errors.add :links, "there has to be at least one link" if links.blank?
  end

  def generate_identifier
    adjs = [
      "autumn", "hidden", "bitter", "misty", "silent", "empty", "dry", "dark",
      "summer", "icy", "delicate", "quiet", "cool", "spring", "winter",
      "patient", "twilight", "dawn", "wispy", "weathered",
      "billowing", "broken", "cold", "damp", "falling", "frosty",
      "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
      "rough", "still", "small", "sparkling", "throbbing", "shy",
      "wandering", "withered", "wild", "young", "holy", "solitary",
      "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
      "polished", "ancient", "lively", "nameless",
      "waterfall", "river", "breeze", "moon", "rain", "wind", "sea", "morning",
      "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn", "glitter",
      "forest", "hill", "cloud", "meadow", "sun", "glade", "bird", "brook",
      "butterfly", "bush", "dew", "dust", "field", "fire", "flower", "firefly",
      "feather", "grass", "haze", "mountain", "night", "pond", "darkness",
      "snowflake", "silence", "sound", "sky", "shape", "surf", "thunder",
      "violet", "water", "wildflower", "wave", "water", "resonance", "sun",
      "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
      "frog", "smoke", "star"
    ]
    self.identifier ||= [Faker::Commerce.color, Faker::Address.street_name.split(' ').first, adjs.sample].join("-").downcase
  end

  def url(host)
    "http://#{host}/#{identifier}"
  end
end
